import { Component } from '@angular/core';
import { SocketProviderConnect } from './chat.service';
import { CookieService } from 'ngx-cookie-service';
import { faSmile, faPaperPlane } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  faSmile = faSmile;
  faPaperPlane = faPaperPlane;
  title = 'uiMyChat';
  nickname: any;
  userId: any;
  msg: any;
  inputMessage: any;
  showMessage: any;
  messages = [];
  users = [];

  constructor(protected socketService: SocketProviderConnect, private cookieService: CookieService) {
    socketService.outEven.subscribe(res => {
      let i = -1;
      const id = socketService.ioSocket.id;
      switch (res.event) {
        case 'message':
          this.messages.push({ ...res, myId: id });
          break;
        case 'users':
          this.users = res.users;
          break;
        case 'addUser':
          this.users.push(res.user);
          break;
        case 'editUser':
          i = this.users.findIndex( u => (u.id === res.user.id ));
          if (i > 0 && this.users[i]) {
            this.users[i].nickname = res.user.nickname;
          }
          break;
        case 'deleteUser':
          i = this.users.findIndex( u => (u.id === res.user.id ));
          if (i > 0 && this.users[i]) {
            delete this.users[i];
          }
          break;
      }
    });
  }

  getUniqueId(parts: number): string {
    const stringArr = [];
    for (let i = 0; i < parts; i++){
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      stringArr.push(S4);
    }
    return stringArr.join('-');
  }

  setUser = () => {
    this.cookieService.set('user', JSON.stringify({
      nickname: this.nickname ,
      id: this.userId
    }));
  }

  changeUser = () => {
    this.socketService.emitEvent('changeUser',
      {
        id: this.userId,
        nickname: this.nickname
      });
  }

  sendMessage = () => {
    this.socketService.emitEvent('message',
      {
        message: this.inputMessage
      });
    this.inputMessage = null;
  }

  ngOnInit() {
    try{
      this.showMessage = JSON.parse(this.cookieService.get('user'));
    }catch (e) {
      this.showMessage = {
        nickname: `user-${this.getUniqueId(1)}`,
        id: this.getUniqueId(6)
      };
      this.cookieService.set('user', JSON.stringify({
        nickname: this.showMessage.nickname,
        id: this.showMessage.id
      }));
    }
    this.users.push(this.showMessage);

  }
}
