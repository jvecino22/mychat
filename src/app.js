const express = require('express')
const app = express();
const wsServer = require('http').Server(app);
const io = require('socket.io')(wsServer);
const chalk = require('chalk');

const wsPort = process.env.WSPORT || 3100;
const httpPort = process.env.HTTPPORT || 8080;
const publicFolder = process.env.PUBLICPATH || 'uiMyChat/dist/uiMyChat';
const users = {};
const room = 'default';

io.on('connection', (socket) => {
    const id = socket.id;

    let { payload } = socket.handshake.query;
    console.log(`${chalk.blue(`Nuevo dispositivo conectado: ${id}`)}`);

    if (payload) {
        payload = JSON.parse(payload)

        socket.join(room);

        console.log(`${chalk.yellow(`El dispositivo ${id} se unio a -> ${room}`)}`);

        users[id]= payload;
        socket.emit('default', { event:'users', users })
        io.to(room).emit('default', { event: 'addUser', ...payload });
    }
    socket.on('default', function(res){
        const user = res && res.cookiePayload ? JSON.parse(res.cookiePayload): {};
        const inPayload = res ? res.payload: {};

        switch (res.event) {
            case 'message':
                io.to(room).emit(res.event,{ id, event: res.event, user, msg: inPayload.message });
                break;
            case 'changeName':
                io.to(room).emit(res.event,{ event: res.event, user });
                break;
            default:
                /** Otros posibles casos */
                break;
        }

    });
    socket.on('disconnect', function () {
        delete users[id]
        io.to(room).emit('default', {event:'deleteUser',  id });
        console.log('user disconnected: ', id);
    });
});



app.use(express.static(publicFolder));

app.listen(httpPort, () => {
    console.log(`HTTP Started port: ${httpPort}`);
});

wsServer.listen(wsPort, () => {
    console.log(`WS Started port: ${wsPort}`);
});
